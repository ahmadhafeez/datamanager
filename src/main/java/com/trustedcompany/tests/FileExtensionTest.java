package com.trustedcompany.tests;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jayway.restassured.RestAssured;
import com.trustedcompany.datamanager.FileUploadService;
import com.trustedcompany.fileutility.FileUtility;

import groovyx.net.http.ContentType;

public class FileExtensionTest {
	
	public FileUtility fileUtility;
	 @Before
	 public void setUp() {
		
		 fileUtility = new FileUtility();
 	 }
	 
	/* to use this test case a few changes are required that i made earlier in order to test the class results were green*/
	@Test
	public void checkFileType() {
		assertSame(fileUtility.isAllowedExt("Ahmad.jpeg"),true);
		assertSame(fileUtility.isAllowedExt("Ahmad.jpeg123"),false);
	}
}
