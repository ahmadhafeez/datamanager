package com.trustedcompany.tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;

import com.jayway.restassured.RestAssured;
import com.trustedcompany.datamanager.FileUploadService;
import com.trustedcompany.fileutility.*;

import groovyx.net.http.ContentType;

import static com.jayway.restassured.RestAssured.expect;
public class checkFileUpload {

	 @Before
	 public void setUp() {
		
		  RestAssured.basePath = "http://localhost:8080/DataManager/";
 	 }
	 
	/* to use this test case a few changes are required that i made earlier in order to test the class results were green*/
	@Test
	public void checkInput() {
		 expect().statusCode(200).contentType(ContentType.TEXT).when()
         .get("FileServices/Ahmad/test.txt");
		
	}
	 
  

}
