package com.trustedcompany.fileutility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;

import antlr.collections.List;

public class FileUtility {

	/*
	 * @params encodedFile Base 64 encoded data inside file fileName complete
	 * Filename to store, current context to get relative path userName - this
	 * is a cheat here. will store Path/userName/file this way we dont have to
	 * store the reference into the database and will be able to fetch data
	 * based upon userName in this current test i am passing it as empty and
	 * handling the logic but this can be used in future.
	 */
	private String message;

	public boolean fileUpload(String encodedFileContent, String fileName, String userName, String flag) {

		if(!isAllowedExt(fileName)){
			setMessage("Extension Not Allowed");
			return false;
		}
		File folderPath;
		Properties prop = new Properties();
		InputStream input = null;

		try {

			String filename = "path.PROPERTIES";
			input = getClass().getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				setMessage("Sorry, unable to find " + filename);
				return false;
			}
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;

		}
		

		if (flag != null && flag.equalsIgnoreCase("W")) {

			folderPath = new File(prop.getProperty("windowpath") + userName + "/");
		} else if (flag != null && flag.equalsIgnoreCase("L")) {

			folderPath = new File(prop.getProperty("linuxpath")  + userName + "/");
			// i am on a windows env this needs to be tested.
		} else {
			return false;
		}

		if (!folderPath.exists()) {
			try {
				folderPath.mkdir();
			} catch (SecurityException se) {
				setMessage("Security Exception occured On Windows 7 and plus this may occur");
				return false;
				// TO-DO a logic to handle such error
			}
		}
		String path = folderPath.getAbsolutePath();
		File file = new File(path + "/" + fileName);
		try {
			byte[] bytes = Base64.getDecoder().decode(encodedFileContent);
			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream fop = new FileOutputStream(file);
			fop.write(bytes);
			fop.flush();
			fop.close();
		} catch (IOException ex) {
			setMessage("Input/Output Exeption Occur");
			// TO-DO a logic to handle such error
			ex.printStackTrace();
			return false;
		} catch (IllegalArgumentException ie) {
			setMessage("Illegal Argument Exception Occur");
			ie.printStackTrace();
		}

		return true;

	}
	
	public boolean isAllowedExt(String fileName){
		ArrayList allowedList =  new ArrayList();
		allowedList.add("mp3");
		allowedList.add("jpeg");
		allowedList.add("txt");
		allowedList.add("JPEG");
		allowedList.add("jpg");
		
		String ext = FilenameUtils.getExtension(fileName);
		if(allowedList.contains(ext)){
	
			return true;
		}else{
			
			return false;
		}
		
	}

	public String getMessage() {
		return message;
	}

	private void setMessage(String message) {
		this.message = message;
	}
}
