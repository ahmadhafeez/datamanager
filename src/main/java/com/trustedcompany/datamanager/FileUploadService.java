package com.trustedcompany.datamanager;
 
import java.util.Base64;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.trustedcompany.fileutility.FileUtility;

import org.json.JSONException;
import org.json.JSONObject;
 
@Path("/FileServices")
public class FileUploadService {
	
	// This is the main tool that i have built that is a seperate file upload handler for now there is no such restriction but it can be added.
	FileUtility fileUtility;
	@POST
	@Path("/upload")
	public Response getMsg(String fileObject) {
		JSONObject data = new JSONObject(fileObject);
		boolean flag = false;
		FileUtility fUtil = new FileUtility();
		// param name forTesting please refer to FileUtility Tool for details
		try{
			flag = fUtil.fileUpload(data.getString("fileString"),data.getString("fileName"), "forTesting","W");
		}catch(JSONException jx){
			jx.printStackTrace();
			return Response.status(400).entity("fileString And fileName Params Must be Present").build();
			
		}
		String output="false";
		if(flag){
			output="true";
		}else{
			// This method would tell us whats the error.
			System.out.println(fileUtility.getMessage());
			
		}
		return Response.status(200).entity(output).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
 
	}
	
	
	
 
}